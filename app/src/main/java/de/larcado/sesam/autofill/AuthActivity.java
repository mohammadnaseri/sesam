package de.larcado.sesam.autofill;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.assist.AssistStructure;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;

import de.larcado.sesam.R;
import de.larcado.sesam.Sesam;
import de.larcado.sesam.model.Domain;
import de.larcado.sesam.model.JsonStorage;
import de.larcado.sesam.model.Profil;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.autofill.AutofillManager.EXTRA_ASSIST_STRUCTURE;
import static android.view.autofill.AutofillManager.EXTRA_AUTHENTICATION_RESULT;

public class AuthActivity extends Activity {

    private Intent mReplyIntent;
    private JsonStorage jsonStorage;
    private Spinner domainSpinner;

    static IntentSender getAuthIntentSenderForResponse(Context context) {
        final Intent intent = new Intent(context, AuthActivity.class);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)
                .getIntentSender();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        jsonStorage = JsonStorage.load(this);

        final EditText masterpwd = findViewById(R.id.masterpwd);
        domainSpinner = findViewById(R.id.domain);
        refreshSpinnerItems();

        findViewById(R.id.autofillInfo).setVisibility(View.VISIBLE);
        TextView clickOnPasswordTextView = findViewById(R.id.clickOnPasswordText);
        clickOnPasswordTextView.setText("Click on password to use it for autofill");

        final Button button = findViewById(R.id.button);
        final TextView pwd = findViewById(R.id.pwd);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "NotoSerif-Italic.ttf");
        pwd.setTypeface(myTypeface);

        final ProgressBar progress = findViewById(R.id.progress);
        final TextView details = findViewById(R.id.detail);

        button.setOnClickListener(v -> {
            final Domain selectedDomain = (Domain) domainSpinner.getSelectedItem();
            final Profil selectedProfil = jsonStorage.getProfilById(selectedDomain.getProfilID());
            final String s_masterpwd = masterpwd.getText().toString();

            details.setText("");
            pwd.setText("please wait...");

            if (!s_masterpwd.equals("")) {
                button.setEnabled(false);
                selectedDomain.setUsecount(selectedDomain.getUsecount() + 1);
                Collections.sort(jsonStorage.getDomains());
                jsonStorage.save(this);
                refreshSpinnerItems(selectedDomain);
                Observable.create((ObservableOnSubscribe<String>) e -> {
                    e.onNext(Sesam.getPasswordSesam(AuthActivity.this, s_masterpwd, selectedDomain, selectedProfil, progress, details));
                    e.onComplete();
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(s -> {
                            pwd.setText(s);
                            button.setEnabled(true);
                        });
            } else {
                Toast.makeText(AuthActivity.this, "You must specify a master password and a domain", Toast.LENGTH_SHORT).show();
            }
        });

        pwd.setOnClickListener(v -> {
            Intent intent = getIntent();
            AssistStructure structure = intent.getParcelableExtra(EXTRA_ASSIST_STRUCTURE);

            mReplyIntent = new Intent();
            mReplyIntent.putExtra(EXTRA_AUTHENTICATION_RESULT,
                    AutofillHelper.newResponse(this, structure, pwd.getText().toString())
            );
            finish();
        });
    }

    private void refreshSpinnerItems() {
        refreshSpinnerItems(null);
    }

    private void refreshSpinnerItems(Domain selectedDomain) {
        jsonStorage = JsonStorage.load(this);
        ArrayAdapter<Domain> spinnerAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                jsonStorage.getDomains()
        );
        domainSpinner.setAdapter(spinnerAdapter);
        if (selectedDomain != null) {
            for (int i = 0; i < jsonStorage.getDomains().size(); i++) {
                Domain d = jsonStorage.getDomains().get(i);
                if (d.getName().equals(selectedDomain.getName())) {
                    domainSpinner.setSelection(i, false);
                    return;
                }
            }
        }
    }

    @Override
    public void finish() {
        if (mReplyIntent != null) {
            setResult(RESULT_OK, mReplyIntent);
        } else {
            setResult(RESULT_CANCELED);
        }
        super.finish();
    }
}