package de.larcado.sesam.autofill;

import android.app.assist.AssistStructure;
import android.content.IntentSender;
import android.os.CancellationSignal;
import android.service.autofill.AutofillService;
import android.service.autofill.FillCallback;
import android.service.autofill.FillContext;
import android.service.autofill.FillRequest;
import android.service.autofill.FillResponse;
import android.service.autofill.SaveCallback;
import android.service.autofill.SaveRequest;
import android.support.annotation.NonNull;
import android.view.autofill.AutofillId;
import android.widget.RemoteViews;

import java.util.List;

import de.larcado.sesam.R;

public class MyAutofillService extends AutofillService {


    @Override
    public void onFillRequest(@NonNull FillRequest request, @NonNull CancellationSignal cancellationSignal, @NonNull FillCallback callback) {

        // Get the structure from the request
        List<FillContext> context = request.getFillContexts();
        AssistStructure structure = context.get(context.size() - 1).getStructure();

        // Disabling autofill for components in this app.
        if (structure.getActivityComponent().toShortString()
                .contains("de.larcado.sesam")) {
            callback.onSuccess(null);
            return;
        }

        // Parse AutoFill data in Activity
        StructureParser parser = new StructureParser(structure);
        List<AssistStructure.ViewNode> viewNodes = parser.parse();
        if (viewNodes.size() > 0) {
            AutofillId[] autofillIds = viewNodes.stream()
                    .map(AssistStructure.ViewNode::getAutofillId)
                    .toArray(AutofillId[]::new);

            FillResponse.Builder responseBuilder = new FillResponse.Builder();
            IntentSender sender = AuthActivity.getAuthIntentSenderForResponse(this);
            RemoteViews presentation = AutofillHelper.newRemoteViews(getPackageName(), getString(R.string.autofill_sign_in_prompt));
            responseBuilder.setAuthentication(autofillIds, sender, presentation);
            callback.onSuccess(responseBuilder.build());
        } else {
            callback.onSuccess(null);
        }
    }

    @Override
    public void onSaveRequest(@NonNull SaveRequest request, @NonNull SaveCallback callback) {

    }
}