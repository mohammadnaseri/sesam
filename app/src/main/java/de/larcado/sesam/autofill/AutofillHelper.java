package de.larcado.sesam.autofill;

import android.app.assist.AssistStructure;
import android.content.Context;
import android.service.autofill.Dataset;
import android.service.autofill.FillResponse;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import android.widget.RemoteViews;

import java.util.List;

import de.larcado.sesam.R;


public class AutofillHelper {

    public static FillResponse newResponse(Context context, AssistStructure structure, String password) {
        // Parse AutoFill data in Activity
        StructureParser parser = new StructureParser(structure);
        List<AssistStructure.ViewNode> viewNodes = parser.parse();
        AutofillId[] autofillIds = viewNodes.stream()
                .map(AssistStructure.ViewNode::getAutofillId)
                .toArray(AutofillId[]::new);

        Dataset dataset = newDataset(context, autofillIds, password);
        return new FillResponse.Builder()
                .addDataset(dataset)
                .build();
    }

    public static Dataset newDataset(Context context, AutofillId[] autofillIds, String password) {
        Dataset.Builder datasetBuilder = new Dataset.Builder
                (newRemoteViews(context.getPackageName(), password));
        for (AutofillId autofillId : autofillIds) {
            datasetBuilder.setValue(autofillId, AutofillValue.forText(password));
        }
        if (autofillIds.length > 0) {
            return datasetBuilder.build();
        } else {
            return null;
        }
    }

    public static RemoteViews newRemoteViews(String packageName, String remoteViewsText) {
        RemoteViews presentation = new RemoteViews(packageName, R.layout.autofill_item);
        presentation.setTextViewText(R.id.infoText, remoteViewsText);
        return presentation;
    }
}