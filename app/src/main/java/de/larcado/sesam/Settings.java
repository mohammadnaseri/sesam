package de.larcado.sesam;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Settings {
    private SharedPreferences sp;

    public Settings(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getLowerCaseLetters() {
        return sp.getString("lower_case_letters", "abcdefghijklmnopqrstuvwxyz");
    }

    public String getUpperCaseLetters() {
        return sp.getString("upper_case_letters", "ABCDEFGHIJKLMNPQRSTUVWXYZ");
    }

    public String getNumbers() {
        return sp.getString("numbers", "0123456789");
    }

    public String getSpecialCharacters() {
        return sp.getString("special_characters", "#!\"§$%&/()[]{}=-_+*~<>,;.:");
    }

    public String getSalt() {
        return sp.getString("salt", "pepper");
    }

    public int getLengthOfPasswords() {
        return Integer.parseInt(sp.getString("length_of_passwords", "20"));
    }

    public int getIterations() {
        return Integer.parseInt(sp.getString("iterations", "64000"));
    }

    public int getHashBitSize() {
        return Integer.parseInt(sp.getString("hash_bit_size", "256"));
    }

    public String getPasswordCharactersWithoutSpecial() {
        return getLowerCaseLetters() + getUpperCaseLetters() + getNumbers();
    }
}