package de.larcado.sesam.utils;

import android.util.Log;

import de.larcado.sesam.BuildConfig;

public class Logger {
    public static final int VERBOSE = Log.VERBOSE;
    public static final int DEBUG = Log.DEBUG;
    public static final int INFO = Log.INFO;
    public static final int WARN = Log.WARN;
    public static final int ERROR = Log.ERROR;
    public static final int ASSERT = Log.ASSERT;

    public static boolean isDebugEnabled() {
        return BuildConfig.DEBUG;
    }

    public static void v(Class clazz, String msg) {
        if (BuildConfig.DEBUG) {
            Log.v(clazz.getName(), msg);
        }
    }

    public static void v(Class clazz, String msg, Throwable e) {
        if (BuildConfig.DEBUG) {
            Log.v(clazz.getName(), msg, e);
        }
    }

    public static void dToString(Class clazz, Object obj) {
        if (BuildConfig.DEBUG) {
            Log.d(clazz.getName(), obj.toString());
        }
    }

    public static void d(Class clazz, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(clazz.getName(), msg);
        }
    }

    public static void d(Class clazz, String msg, Throwable e) {
        if (BuildConfig.DEBUG) {
            Log.d(clazz.getName(), msg, e);
        }
    }

    public static void i(Class clazz, String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(clazz.getName(), msg);
        }
    }

    public static void i(Class clazz, String msg, Throwable e) {
        if (BuildConfig.DEBUG) {
            Log.i(clazz.getName(), msg, e);
        }
    }

    public static void w(Class clazz, String msg) {
        if (BuildConfig.DEBUG) {
            Log.w(clazz.getName(), msg);
        }
    }

    public static void w(Class clazz, String msg, Throwable e) {
        if (BuildConfig.DEBUG) {
            Log.w(clazz.getName(), msg, e);
        }
    }

    public static void e(Class clazz, String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(clazz.getName(), msg);
        }
    }

    public static void e(Class clazz, Throwable e) {
        if (BuildConfig.DEBUG) {
            Log.d(clazz.getName(), "handle Exception", e);
        }
    }

    public static void e(Class clazz, String msg, Throwable e) {
        if (BuildConfig.DEBUG) {
            Log.e(clazz.getName(), msg, e);
        }
    }

    public static void wtf(Class clazz, String msg) {
        if (BuildConfig.DEBUG) {
            Log.wtf(clazz.getName(), msg);
        }
    }

    public static void wtf(Class clazz, String msg, Throwable e) {
        if (BuildConfig.DEBUG) {
            Log.wtf(clazz.getName(), msg, e);
        }
    }

    public static String getStackTraceString(Throwable e) {
        return Log.getStackTraceString(e);
    }

    public static void println(Class clazz, int priority, String msg) {
        if (BuildConfig.DEBUG) {
            Log.println(priority, clazz.getName(), msg);
        }
    }
}