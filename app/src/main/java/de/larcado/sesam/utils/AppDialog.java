package de.larcado.sesam.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import de.larcado.sesam.R;
import io.reactivex.functions.Action;

public class AppDialog {

    private AppDialog() {
    }

    public static void yesNoAlertDialog(Context context, String title, String message, DialogInterface.OnClickListener positive,
                                        DialogInterface.OnClickListener negative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppDialogTheme);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Yes", positive);
        builder.setNegativeButton("No", negative);
        builder.show();
    }

    public static void inputDialog(Context context, String title, EditText input, DialogInterface.OnClickListener okClick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppDialogTheme);
        builder.setTitle(title);
        builder.setView(input);
        builder.setPositiveButton("OK", okClick);
        builder.setNegativeButton("back", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    public static void bearbeitenLoeschenDialog(Activity activity, String title, Action bearbeiten, Action loeschen) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_edit_delete, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppDialogTheme);
        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();
        TextView t_title = (TextView) dialogView.findViewById(R.id.dialogTitle);
        t_title.setText(title);
        dialogView.findViewById(R.id.bearbeiten).setOnClickListener(v -> {
            try {
                bearbeiten.run();
            } catch (Exception e) {
                Logger.e(AppDialog.class, e);
            }
            dialog.dismiss();
        });
        dialogView.findViewById(R.id.loeschen).setOnClickListener(v -> {
            try {
                loeschen.run();
            } catch (Exception e) {
                Logger.e(AppDialog.class, e);
            }
            dialog.dismiss();
        });
        dialog.show();
    }
}
