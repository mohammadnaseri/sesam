package de.larcado.sesam;

import android.graphics.drawable.Icon;
import android.os.Build;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;
import android.support.annotation.RequiresApi;

import de.larcado.sesam.utils.Logger;

@RequiresApi(api = Build.VERSION_CODES.N)
public class AppTileService extends TileService {

    @Override
    public void onStartListening() {
        Logger.d(getClass(), "onStartListening()");
        updateTile();
    }

    @Override
    public void onClick() {
        if (isLocked()) {
            Logger.d(getClass(), "onClick() notSecure");
        } else {
            Logger.d(getClass(), "onClick() secure");
            NotificationHandler.createInsertMasterPw(this);
        }
    }

    @Override
    public void onStopListening() {
        Logger.d(getClass(), "onStopListening()");
        updateTile();
    }

    private void updateTile() {
        Logger.d(getClass(), "updateTile()");
        Tile tile = getQsTile();
        if (tile != null) {
            tile.setIcon(Icon.createWithResource(this, R.drawable.tile_icon));
            tile.setLabel(getString(R.string.app_name));
            if (isLocked()) {
                Logger.d(getClass(), "updateTile() notSecure INACTIVE");
                tile.setState(Tile.STATE_INACTIVE);
            } else {
                Logger.d(getClass(), "updateTile() secure ACTIVE");
                tile.setState(Tile.STATE_ACTIVE);
            }
            tile.updateTile();
        }
    }
}
