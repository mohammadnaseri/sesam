package de.larcado.sesam;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.larcado.sesam.model.Profil;
import de.larcado.sesam.utils.Logger;
import io.reactivex.functions.Consumer;

public class ProfilAdapter extends RecyclerView.Adapter<ProfilAdapter.MyViewHolder> {

    private List<Profil> profilList;
    private Consumer<Profil> onClick;
    private Consumer<Profil> onLongClick;
    private Typeface myTypeface;

    public ProfilAdapter(Context context, List<Profil> profilList, Consumer<Profil> onClick, Consumer<Profil> onLongClick) {
        this.profilList = profilList;
        this.onClick = onClick;
        this.onLongClick = onLongClick;

        myTypeface = Typeface.createFromAsset(context.getAssets(), "NotoSerif-Italic.ttf");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_profil, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Profil profil = profilList.get(position);
        holder.name.setText(profil.getName() + " (" + profil.getLength() + ")");
        holder.sonderzeichen.setTypeface(myTypeface);
        holder.sonderzeichen.setText(profil.getSonderzeichen());

        holder.root.setOnClickListener(v -> {
            try {
                onClick.accept(profil);
            } catch (Exception e) {
                Logger.e(this.getClass(), e);
            }
        });

        holder.root.setOnLongClickListener(v -> {
            try {
                onLongClick.accept(profil);
                return true;
            } catch (Exception e) {
                Logger.e(this.getClass(), e);
            }
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return profilList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public View root;
        public TextView name, sonderzeichen;

        public MyViewHolder(View view) {
            super(view);
            root = view;
            name = view.findViewById(R.id.name);
            sonderzeichen = view.findViewById(R.id.sonderzeichen);
        }
    }
}